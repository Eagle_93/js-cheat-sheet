/* JavaScript Cheat Sheet

===JAVASCRIPT(JS)===:

   -Is a lightweight, interperted or, just-in-time compiled programming language.

   -While it is most well known as the scripting language for Web pages, many non-browser environments such as Node.js, Apache, CouchDB, and Adobe Acrobat also use it.

   -JavaScript is a prototype-based, multi-paradigm, single-threaded, dynamic language, supporting Object-Oriented, Imperative, and Declarative(i.e. Functional Programming) styles.

   -In 2015 ECMA International published the 6th version of JS known as ES6. (This Cheat Sheet refers to teh curent version of JS known as ECMAScript 2020).


===BASIC RULES OF JAVASCRIPT===
 (The following irules apply to the basic syntax(rules) of JS):

==  {JavaScript is case-sensitive}, (If the same case letters are not used for a variable etc... then JS will recognize that as a new variable).
    
==  {STATEMENTS: should end with a semicolon (;)}.

==  {VARIABLES}: must be defined before being used}, (The variable name can contain A - Z, a - z, underscore or digits, and must start with a letter or underscore (_)).

-   Assume the type of data that is put into the variable. The data type does not have to be explicitly defined)
-   Variables are golbal variables when difined outside of a function, and are available anywhere in the current script contect.
-   Variables created within a function are local variables and can be used only within that function.

===STRINGS===

-   Strings have to be enclosed in single ('') or double ("") quotes to be recognized as a string.
~Example*/

print('Hello World', "Hello World"); //produces the following: Hello World , Hello World; because both are enclosed in either single ot double quotes.

/*

-   Special Characters that are displayed literally must be preceded by a backslash character ( \ ).
-   To place quotations within a string, you must place the backslash before each quote you wish to recognize and print within the string:
~Example:*/

print("She asked me,\"Why did you not get milk while you were at the store\"");

/*

===Incrementation of a variable===: is done by using two + symbols (++, i.e. 1++ will keep incrementing 1 until infinity unless something is put in place to stop it!)

===Decrementation of a variable===: is done by using two - symbols(--, i.e. 5--), and the same effect will take place as incrementation to infinity.

===Comments(single-line)===: are entered into the script by typing two (//).

===Comments(double-line)===: are entered into the script by typing (/* to start the comment, then to end the multi-line comment do */ 
/*

===Values that are not defined as a data type (string, boolean, number) may be defined as an object such as: Date, Array, Boolean, String and Number. As an example you could define:*/

var arrayList = new Array("Test", "this", "list");

/*

===Dots in Service Manager Field Names===: must be replaced by an underscore (_) in JS. i.e. contact.nae becomes contact_name.

===Service Manager Field names===: that are reversed words in JS have to be preceded by an underscore such as, _class for the class field.

===This is a summary of the standard JS rules. Now g install it and give it a try!


=====RULES=====

-Use 2 spaces for indentation:*/
       function hello (name) {
           console.log('hi', name)
       };
/*
-Use single quotes for strings except to avoid escaping during a double quote string:*/
       console.log('hello there')     //correct form!
       console.log(`hello ${name}`)   //Is OK Too
       console.log("hello there")    // AVOID THIS!
       console.log(`hello there`)     //AVOID THIS TOO!
 /*      
-No Unused Variables:*/
       function myFunction () {
           var result = something()   // AVOID THIS!
       };
/*
-Add space after keywords:*/
       if (condition) {}   //THIS IS GOOD!
       if(condition)  {}   //AVOID THIS!
/*
-Add a space before a function declaration's parenthesis:*/
       function name (arg) {}   //GOOD!
       function name(arg) {}   //AVOID THIS!

       run(function () {})   //GOOD!
       run(function() {})   //AVOID THIS!
/*
-Always use === instead of ==
          (EXCEPTION: obj)
       == null is allowed to check for null.
       || undefined.*/
              if (name === 'John')   //GOOD!
              if (name == 'John')   //AVOID THIS!
              if (name !== 'John')   //GOOD!
              if (name != 'John')   //AVOID THIS!
/*
-Infix Operators must be spaced:*/
       var x = 2;   //GOOD!
       var message = 'hello, ' + name + '!';   //GOOD!
       var x=2;   //AVOID THIS!
       var message = 'hello, '+name+'!';   //AVOID THIS!
/*
-Commas should have a space after them:*/
       var list = [1, 2, 3, 4];   //GOOD!
       function greet (name, options) {}   //GOOD!

       var list = [1,2,3,4];   //AVOID THIS!
       function greet (name,options) {}   //AVOID THIS!
/*
-Keep else statements on the same line as curly braces:*/
       if (condition) {
         //etc...
       } else {
           //etc...
       };   //GOOD!

       if (condition) {
           //etc...
       }
       else {
           //etc...
       };   //AVOID THIS!
/*
-For multi-line if statements, use curly braces:*/
       if (options.quiet !== true)
       console.log('done');   //GOOD!

       if (options.quiet !== true)
       {
              console.log('done')
       };   //GOOD!

       if (options.quiet !== true)
          console.log('done');   //AVOID THIS!
/*
-// Declare browser globals with a multi-line comment /* ... */ /*
exceptions are: window, document, navigator. Prevents accidental use of poorly-named globals like open, length, event, and name.*/
       /*global alert prompt*/
       alert('hi')
       prompt('ok?');
//...explicitly referencing the function or property is ok too, though such code will not run in a "Worker" which uses 'self' instead of 'window'.*/
     window.alert('hi');   //GOOD!
/*
-Multiple blank lines are not allowed.*/
       var value = 'hello world'
       console.log(value);   //GOOD!

       var value = 'hello world'


       console.log(value);   //AVOID THIS!
/*
-For the Ternary operator in a multi-line setting, place '?' and ':' on their own lines:*/
       var location = env.development ? 'localhost' : 'www.api.com';   //GOOD!

       var location = env.development
       ? 'localhost'
       : 'www.api.com'   //GOOD!

       var location = env.development
       ?
       'localhost' :
       'www.api.com';   //AVOID THIS MESS!
/*
-Wrap conditional assignments with additional parenthesis. This makes it clear that the expression is intentionally an assignment (=) rather than a typo for equality (===).*/
       while (((m = text.matchexpr))) {
              //etc...
       };   //GOOD!

       while (m = text.match(expr))
       {
              //etc...
       };   //AVOID THIS!
/*
-Add spaces inside single line blocks. */
       function foo () {return
       true};   //AVOID THIS!

       function foo () { return true };   //GOOD!
/*
-Use camelcase when naming variables and functions:*/
       function myFunction () {
              //etc...
       };   //GOOD!

       function my_function () {
              //etc...
       };   //AVOID THIS!
/*
-Trailing commas not allowed:*/
       var obj = {
              foo: 'foo',
              bar: 'bar'
       };   //GOOD!

       var obj = {
              foo: 'foo'
              ,bar: 'bar'
       };   //AVOID THIS!
/*
-Dot should be on the same line as property:*/
       console.log('hello');   //GOOD!
        
       console
        .log('hello');   //GOOD!

        console
        .log('hello');   //AVOID THIS!
/*
-Add space between colon and value in key value pairs:*/
       var obj = { 'key': 'value'};   //GOOD!
       var obj = { 'key' : 'value'};   //AVOID THIS!
       var obj = { 'key':'value'};   //AVOID THIS!
       var obj = { 'key' :'value'};   //AVOID THIS!
/*
-Constructor names must begin with a capital letter:*/
       function animal () {}
       var dog = new animal ();   //AVOID THIS!

       function Animal () {}
       var dog = new Animal();   //GOOD!
/*
-Constructor with no arguments must be invoked with parenthesis:*/
       function Animal () {}
       var dog = new Animal;   //AVOID THIS!

       var dog = new Animal();   //GOOD!
/*
-Objects must contain a getter when a setter is defined:*/
       var person = {
              set name (value) {
                     this._name = value
              },
              get name () {
                     return this._name
              }
       };   //GOOD!

       var person = {
              set name (value) {
                     this._name = value
              }
       };   //AVOID THIS!
/*
-Constructors of derived clases mnust call super:*/
       class dog extends Animal {
              constructor () {
                     super()
                     this.legs = 4
              }
       };   //GOOD!

       class Dog {
              constructor () {
                     super()
                     this.legs = 4
              }
       };   //AVOID THIS!
/*
-Use array literals instead of array constructors:*/
       var nums = [1, 2, 3];   //GOOD!
       var nums = new Array (1, 2, 3);   //AVOID THIS!
/*
-Avoid modifying variables of class declarations:*/
       class DOg {}
       Dog = 'Fido';   //AVOID THIS!
/*
-Avoid modifying cvariables declared using const:*/
       const score = 100
       score = 125;   //AVOID THIS!
/*
-Avoid using constant expressions in conditions(except loops):*/
       if (x === 0) {
              //etc...
       };   //GOOD!
       while (true) {
              //etc...
       };   //GOOD!

       if (false) {
              //etc...
       };   //AVIOD THIS!
/*
-No control characters in regular expressions:*/
       var pattern = /\x20/;   //GOOD!
       var pattern = /\1f/;   //AVOID THIS MESS!
/*
-No debugger statements:*/
       function sum (a, b) {
              debugger ; return a + b
       };   //AVOID THIS!
/*No delete operators on variables:*/
       var name
       delete name;   //AVOID THIS!
/*
-No duplicate arguments in function definitions:*/
       function sum (a, b, a) {
              //etc...
       };   //AVOID THIS!

       function sum (a, b, c) {
              //etc...
       };   //GOOD!
/*
-No duplicate name in class members:*/
       class Dog {
              bark() {}
              bark() {}
       };   //AVOID THIS!
/*
-No duplicate keys in object literals:*/
       var user = {
              name: 'Jane Doe',
              name: 'John Doe'
       };   //AVOID THIS!
/*
-No duplicate case labels in switch statements:*/
       switch (id) {
              case 1:
                     //etc...
              case 1:
       };   //AVOID THIS!
/*
-use a single import statement per module:*/
       import { myFunc1 } from 'module'
       import { myFunc2 } from 'module';   //AVOID THIS!

       import { myFunc1, myFunc2 } from 'module';   //GOOD!
/*
-No empty character classes in regular expressions:*/
       const myRegEx = /^abc[]/;   //AVOID THIS!
       const myRegEx = /^abc[a-z]/;   //GOOD!
/*
-No empty destructuring patterns:*/
       const { a: {} } = foo;   //AVOID THIS!
       const { a: { b } } = foo;   //GOOD!
/*
-No using eval:*/
       eval( "var result = user." +
       propName );   //AVOID THIS MESS!

       var result = user[propName];   //GOOD!
/*
-No reassigning exceptions in catch clauses:*/
       try {
              //etc...
       } catch (e) {
              e = 'new value'
       };   //AVOID THIS!

       try {
              //etc...
       } catch (e) {
              const newVal = 'new value'
       };   //GOOD!
/*
-No extended native objects:*/
       Object.prototype.age = 21;   //AVOID THIS!
/*
-Avoid unnecessary function binding:*/
       const name = function () {
              getName()
       }.bind(user);   //AVOID THIS!

       const name = function () {
              this.getName()
       }.bind(user);   //GOOD!
/*
-Avoid unnecessary boolean casts:*/
       const result = true
       if (!!result) {
              //etc...
       };   //AVOID THIS!

       const result = trueif (result) , {
              //etc...
       };   //GOOD!
/*
-No unnecessary parenthesis around function expressions:*/
       const myFunc = (function ()
       {});   //AVOID!

       const myFunc = function () {

       };   //GOOD!
/*
-Use Break to prevent fallthrough in switch cases:*/
       switch (filter) {
              case 1:
                     doSomething()

              case 2: doSomethingElse()
       };   //AVOID!

       switch (filter) {
              case 1: doSomething()
              break
              case 2: doSomethingElse()
       };   //GOOD!

       switch (filter) {
              case 1: doSomething()
              //fallthrough // ok
              case 2: doSomethingElse()
       };   //GOOD!
/*
-No floating decimals:*/
       const discount = .5;   //AVOID THIS!
       const discount = 0.5;   //GOOD!
/*
-Avoid reassigning function declarations:*/
       function myFunc () {}
       myFunc = myOtherFunc;   //AVOID!
/*
-No reassigning read-only global variables:*/
       window = {};   //AVOID!
/*
-No implied eval():*/
       setTimeout("alert('Hello world')");   //AVOID!
       setTimeout(function () {
              alert('Hello world')
       });   //GOOD!
/*
-No function declarations in nested blocks:*/
       if (authenticated) {
              function setAuthUser () {

              }
       };   //AVOID!
/*
-No invalid regualar expression strings in RegEx constructors:*/
       RegExp('[a-z');   //AVOID!
       RegExp( '[a-z]' );   //GOOD!
/*
-No irregular whitespace:*/
       function myFunc () /**/{};   //AVOID!
/*
-No using __ iterator:*/
       foo.prototype.__iterator__ =
       function () {};   //AVOID!
/*
-No labels that share a name with an in-scope cvariable:*/
       var scope = 100
       function game () {
              score: while(true) {
                     score-+ 10
                     if (score > 0) continue
                     score
                     break
              }
       };   //AVOID THIS MESS!
/*
-No label statements:*/
       label:
       while (true) {
              break label
       };   //AVOID!
/*
-No unnecessary nested blocks:*/
       function myFunc () {
              {
       myOtherFunc()
              }
       };   //AVOID THIS!

       function myFunc () {
              myOtherFunc()
       };   //GOOD!
/*
-Avoid mixing spaces and tabs for indentation:
-DO NOT use multiple spaces except for indentation!*/
       const id =    1234;   //AVOID!
       const id = 1234;   //GOOD!
/*
-No multi-line strings:*/
       //const message = 'Hello 
       //world';   //AVOID THIS!

       const message = 'Hello World';   //GOOD!
/*
-No new without assigning an object to a variable:*/
       new Character();   //AVOID!

       const Character = new Character();   //GOOD!
/*
-No using the function constructor:*/
       var sum = new Function ('a',
       'b', return a + b;);   //AVOID THIS MESS!
/*
- No using the Object constructor:*/
       let config = new Object();   //AVOID THIS!
/*
-No using 'new require':*/
       const myModule = new require
       ('my-module');   //AVOID!
/*
-No using the symbol constructor:*/
       const foo = new Symbol ('foo');   //AVOID!
/*
-No using primitive wrapper instances:*/
       const mesage = new String
       ('Hello');   //AVOID!
/*
-No calling global object properties as functions:*/
       const math = Math();   //AVOID!
/*
-No octal literals:*/
       const octal = 042;   //AVOID!
       const decimal = 34;   //GOOD!
       const octalString = '042';   //GOOD!
/*
-No octal escape sequences in string literals:*/
       const copywrite = 'Copywrite \251';   //AVOID!
/*
-Avoid string concatenation when using __dirname and __filename:*/
       const pathToFile = __dirname + '/app.js';   //AVOID!
       const pathToFile = path.join(__dirname, 'app.js')   //GOOD!
/*
-Avoid using __proto__, instead use getPrototype0:*/
       const foo = obj.__proto__;   //AVOID!
       const foo = Object.getPrototypeOf(obj);   //GOOD!
/*
-No redaclaring variables:*/
       let name = 'John'
       let name = 'Jane';   //AVOID!

       let name = 'John'
       name = 'Jane';   //GOOD!
/*
-Avoid multiple spaces in regular expression literals:*/
       const regexp = /test   value/;   //AVOID!
       const regexp = /test value/;   //GOOD!
       const regexp = /test {3}value/;   //GOOD!
/*
-Assignments in return statements must be surrounded by parenthesis:*/
       function sum (a, b) {
              return result = a + b
       };   //AVOID!

       function sum (a, b) {
              return (result = a + b)
       };   //GOOD!
/*
-Avoid assigning a variable to itself:*/
       name = name;   //AVOID!
/*
-Avoid comparing a variable to itself:*/
       if (score === score) {};   //AVOID!
/*
-Avoid using the comma operator:*/
       if (doSomething(), !!test)
       {};   //AVOID!
/*
-Restricted names should not be shadowed:*/
       let undefined = 'value';   //AVOID!
/*
-Sparse arrays are not allowed:*/
       let fruits = ['apple',    'orange'];   //AVOID!
/*
-Tabs shoulds not be used.
-Regular strings must not contain template literal placeholders:*/
       const message = 'Hello ${name}';   //AVOID!
       const message = `Hello ${name}`;   //GOOD!
/*
-Super() must be called before using "this";*/
       class Dog extends Animal {
              constructor () {
                     this.legs = 4
                     super()
              }
       };   //AVOID!
/*
-Never only "throw" an error object*/
       throw 'error';   //AVOID!

       throw new Error('error');   //GOOD!
/*
-Whitespacing is not allowed at the end of lines(no extra spaces at the end of lines       ).
-Initializing to undefined is not allowed:*/
       let name = undefined;   //AVOID!

       let name = 'value';   //GOOD!
/*
-No unmodified conditions of loops:*/
       for (let i = 0; i < items.length; j++) { ... };   //AVOID!

       for (let i = 0; i < items.length; i++) { ... };   //GOOD!
/*
-No ternary operators when simpler alternatives exist:*/
     let score = val ? val : 0;   //AVOID!

     let score = val || 0;   //GOOD!
/*
-No unreachable code after "return', "throw", "continue", "break" statements:*/
       function doSomething () {
              return true
              console.log('never called')
       };   //AVOID!
/*
-No flow control statements after "finally" blocks:*/
       try {
         //etc...
       } catch (e) {
              //etc...
       } finally {
              return 42
       };   //AVOID!
/*
-The left operand of relations operators must not be negated:*/
       if (!key in obj) {};   //AVOID!
       if (!(key in obj)) {};   //GOOD!
/*
-Avoid unnecessary use of ".call()", and ".apply()" operators:*/
       sum.call(null, 1, 2, 3);   //AVOID!
/*
-Avoid using unnecessary computed property keys on objects:*/
       const user = { ['name']: 'John Doe' };   //AVOID!
       const user = { name: 'John Doe' };   //GOOD!
/*
-No unnecessary constructor:*/
       class Car {
              constructor () {

              }
       };   //AVOID!
/*
-No unnecessary use of escape:*/
       let message = 'Hell\o';   //AVOID THIS MESS! lol
/*
-Renaming import, export and destructured assignments to the same name is NOT allowed:*/
       import { config as config }
       from './config';   //AVOID!

       import { config } from './config';   //GOOD!
/*
-No whitespace before properties:*/
       user. name;   //AVOID!
       user.name;   //GOOD!
/*
-No using "with" statements:*/
       with (val) { ... };   //AVOID!
/*
-Maintain consistency of new lines between object properties:*/
       const user = {
              name: 'Jane Doe', age: 30,
              username: 'jdoe86';
       };   //AVOID!

       const user = { name: 'Jane Doe', age: '30', unsername: 'jdoe86'};   //GOOD!
       
       const user = {
              name: 'Jane Doe',
              age: '30',
              username: 'jdoe86'
       };   //BEST!
/*
-No padding within blocks(extra spaces, returns, or new lines):*/
       if (user) {

              
              const name = getName()
       };   //AVOID!

       if (user) {
              const name = getName()
       };   //GOOD!
/*
-No whitespace between spread operators and their expressions:*/
       function(...   args);   //AVOID!
       function(...args);   //GOOD!
/*
-Semicolons must have a space after, but not before using them:*/
       for (let i = 0  ;i < items.length  ;i++) { ... };   //AVOID!
       for (let i = 0; i < items.length; i++) { ... };   //GOOD!
/*
-Must have a space before blocks:*/
       if (admin){...};   //AVOID!
       if (admin) {...};   //GOOD!
/*
-No spaces inside parenthesis:*/
       getName( name );   //AVOID!
       getName(name);   //GOOD!
/*
-Unary operators must have a space after them:*/
       typeof!admin;   //AVOID!
       typeof !admin;   //GOOD!
/*
-Use spaces inside comments:*/
       //comment// AVOID!
       //*comment*// AVOID!
       // comment // GOOD!
       // *comment *// GOOD!
/*
-No spacing in template strings;*/
       const message = 'Hello, ${ name }';
       const message = 'Hello, ${name}';   //GOOD!
/*
-Use "isNaN()" when checking for Nan:*/
       if (price === Nan) {};   //AVOID!
       if (isNaN(price)) {};   //GOOD!
/*
-"typeof" must be compared to a vild string:*/
       typeof name === 'undefimed';   //AVOID!
       typeof name === 'undefined';   //GOOD!
/*
-Immediately Invoked Function Expressions (IIFEs) must be wrapped:*/
       const getName = function ()
       {}();   // AVOID!

       const getName = (function ()
       {}());   // GOOD!

       const getName = (Function ()
       {}) ();   // GOOD!
/*
-The " * " in yield* expressions must have space before and after use:*/
       yield* increment();   //AVOID!
       yield * increment();   //GOOD!
/*
-Avoid Yada conditions:*/
       if (42 === age) {};   //AVOID!
       if (age === 42) {};   // THIS IS GOOD! > GOOD THIS IS!
/*
-Sometimes no Semicolons are used:*/
       window.alert('hi')   //OK!
       window.alert('hi');   //AVOID!
// This is the only "gotcha" of omitting semicolons and "standard" protects you from this issue.
/*
-Never start a line with "[", "(", "`" or a handful of other unlikely possibilities.
-Most of these will never start a line of code: [, (, `, +, *, /, -, ",", "."*/
       ;(function () {
              window.alert('ok')
       };   // OK!
/*
- Instead of this:*/
       ;[1, 2, 3].forEach(bar);

       // This is strongly preferred:

       var nums = [1, 2, 3]
       nums.forEach(bar);
//